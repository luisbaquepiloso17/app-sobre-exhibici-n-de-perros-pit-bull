package facci.baquejorge.jorgesegundoparcial;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {
    private final int REQUEST_PERMISSION_STORAGE_SAVE = 101;
    private final int REQUEST_PERMISSION_STORAGE_DELETE = 102;


    static final int REQUEST_IMAGE_CAPTURE =1;

    Bitmap imageBitmap;

    private final String FOLDER_NAME = "UOCImageApp";
    private final String FILE_NAME = "imageapp.jpg";

    private Button buttonOpenImage;
    private ImageView imageView;
    private TextView textViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        buttonOpenImage = findViewById(R.id.image_app_btn_capture);
        imageView = findViewById(R.id.image_app_iv_picture);
        textViewMessage = findViewById(R.id.image_app_tv_message);
        buttonOpenImage.setOnClickListener(this);
        cargarImagenSiExiste();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_delete){
            onDeleteMenuTap();
            return true;
        }else if(item.getItemId() == R.id.action_save){
            onSaveMenuTap();

            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private boolean hasPermissionsToWrite(){

        return ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }


    private void onSaveMenuTap() {

        if (!hasPermissionsToWrite()){

            ActivityCompat.requestPermissions(this, new String[]{

                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_SAVE);

        }
        else{

            if(imageView.getDrawable() != null){

                createFolder();
                String storageDir =

                        Environment.getExternalStorageDirectory() + "/UOCImageApp/";
                // createImageFile(storageDir, this, FILE_NAME, imageBitmap);
                createImageFile(storageDir, FILE_NAME, imageBitmap);

            }
            else {

                Toast.makeText(this, "Tome una foto primera", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void onDeleteMenuTap() {
        if (!hasPermissionsToWrite()){

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_DELETE);

        }

        else{

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){

                        case DialogInterface.BUTTON_POSITIVE:
                            try {
                                deleteImageFile();

                            }catch (IOException e){

                                e.printStackTrace();
                            }
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Eliminar Imagen").setMessage("Desea eliminar esta imagen?")
                    .setPositiveButton("SI", dialogClickListener)
                    .setNegativeButton("NO", dialogClickListener).show();
        }

    }


    private void createImageFile( String storageDir, String fileName, Bitmap bitmap){

        try {
            File myFile = new File(storageDir, fileName);
            FileOutputStream stream = new FileOutputStream(myFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();

            Toast.makeText(this, "Imagen guardada", Toast.LENGTH_LONG).show();

        }
        catch (IOException e){

            e.printStackTrace();
        }

    }


    private void deleteImageFile() throws IOException{

        File storageDir = new File(Environment.getExternalStorageDirectory() + "/UOCImageApp/");
        File image = new File(storageDir + "/" + this.FILE_NAME);


        if(image.exists()){
            image.delete();
            Toast.makeText(this, "File Deleted", Toast.LENGTH_LONG).show();
            imageView.setImageResource(0);
            textViewMessage.setVisibility(View.VISIBLE);

        }
        else{

            Toast.makeText(this, "File no found", Toast.LENGTH_LONG).show();
        }

    }

    private void  cargarImagenSiExiste(){

        String myImage = Environment.getExternalStorageDirectory().
                getAbsolutePath()+"/" + FOLDER_NAME + "/" + FILE_NAME;
        File imagen = new File(myImage);
        if (imagen.exists()){

            Picasso.get().load(imagen).into(imageView);
            textViewMessage.setVisibility(View.INVISIBLE);
        }
        else{

            textViewMessage.setVisibility(View.VISIBLE);
        }

    }

    private void createFolder(){
        String myfolder = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+ FOLDER_NAME;

        File folder = new File(myfolder);
        if(!folder.exists())
            if (!folder.mkdir()){

                Toast.makeText(this, FOLDER_NAME +  "no se puede crear", Toast.LENGTH_LONG).show();
            }
            else
                Toast.makeText(this, FOLDER_NAME + "creada exitosamente" , Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {

        if (v == buttonOpenImage){

            dispatchTakePictureIntent();
        }
    }

    public void dispatchTakePictureIntent(){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager()) != null){

            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){

            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap)extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            textViewMessage.setVisibility(View.INVISIBLE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {

        switch (requestCode){

            case REQUEST_PERMISSION_STORAGE_DELETE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }else{

                    Toast.makeText(this, "Permiso de eliminacion denegado", Toast.LENGTH_SHORT).show();
                }
            }

            case REQUEST_PERMISSION_STORAGE_SAVE:{

                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                    createFolder();
                    String storageDir =
                            Environment.getExternalStorageDirectory()+ "/UOCIImageApp/";
                    createImageFile(storageDir, FILE_NAME, imageBitmap);

                }else{

                    Toast.makeText(this, "Permiso de guardar denegado", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}


