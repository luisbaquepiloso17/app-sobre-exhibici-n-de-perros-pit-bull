package facci.baquejorge.jorgesegundoparcial;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import facci.baquejorge.jorgesegundoparcial.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity {
    static int TIMEOUT_MILLIS = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent( MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, TIMEOUT_MILLIS);
    }
}
